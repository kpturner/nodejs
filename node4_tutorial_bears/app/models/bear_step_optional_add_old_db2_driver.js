// app/models/bear.js
// =============================================================================

// BASE SETUP
// =============================================================================

// globals
// > export BEAR_DB_VERSION=1 for db2
// > export BEAR_DB_VERSION=2 for db2a (default)
// =============================================================================
GLOBAL._bear_pool_conn_incr_size = 8;
GLOBAL._bear_database = "*LOCAL";
GLOBAL._bear_schema = "BEARS";
GLOBAL._bear_table = "BEAR";
GLOBAL._bear_db_version = process.env.BEAR_DB_VERSION;
// new improved db2 async
if (GLOBAL._bear_db_version != 1) {
  console.log("new improved db2 async");
  GLOBAL._bear_db = require('/QOpenSys/QIBM/ProdData/OPS/Node4/os400/db2i/lib/db2a');
// moldy old db2 no-async
} else {
  console.log("moldy old db2 no-async");
  GLOBAL._bear_moldy_db = require('/QOpenSys/QIBM/ProdData/OPS/Node4/os400/db2i/lib/db2');
  GLOBAL._bear_moldy_db.init();
  GLOBAL._bear_been_here_done_that = false;
  // GLOBAL._bear_moldy_db.serverMode(true);
  BearOldDriverNotRecommend = function() {
  }
  BearOldDriverNotRecommend.prototype.dbconn = function(p1) {
    this.conn = function(p1) {
      if (! GLOBAL._bear_been_here_done_that) {
        GLOBAL._bear_moldy_db.conn(p1);
        GLOBAL._bear_been_here_done_that = true;
      }
    }
  }
  BearOldDriverNotRecommend.prototype.dbstmt = function(p1) {
    this.exec = function(p1, callback) {
      GLOBAL._bear_moldy_db.exec(p1, callback);
    }
    this.execSync = function(p1, callback) {
      GLOBAL._bear_moldy_db.exec(p1, callback);
    }
  }
  GLOBAL._bear_db = new BearOldDriverNotRecommend();
}

// bear response
//
// HTTP Verb  CRUD            Entire Collection (e.g. /customers)           Specific Item (e.g. /customers/{id})
// =========  ==============  ============================================  =============================================
// POST       Create          201 (Created), 'Location' header with link    404 (Not Found).
//                                to /customers/{id} containing new ID.     409 (Conflict) if resource already exists.
// GET        Read            200 (OK), list of customers. Use pagination,  200 (OK), single customer.
//                                sorting and filtering to navigate         404 (Not Found), if ID not found or invalid.
//                                big lists.
// PUT        Update/Replace  404 (Not Found), unless you want to           200 (OK) or 204 (No Content).
//                                 update/replace every resource            404 (Not Found), if ID not found or invalid.
//                                 in the entire collection.           
// PATCH      Update/Modify   404 (Not Found), unless you want to           200 (OK) or 204 (No Content).
//                                modify the collection itself.             404 (Not Found), if ID not found or invalid.
// DELETE     Delete          404 (Not Found), unless you want to           200 (OK).
//                                delete the whole collection               404 (Not Found), if ID not found or invalid.
// =============================================================================
GLOBAL._bear_msg_10001 = 10001;
GLOBAL._bear_msg_10001_text = "access failed";
GLOBAL._bear_msg_10002 = 10002;
GLOBAL._bear_msg_10002_text = "rows found";
GLOBAL._bear_msg_10003 = 10003;
GLOBAL._bear_msg_10003_text = "no rows found";
GLOBAL._bear_msg_10004 = 10004;
GLOBAL._bear_msg_10004_text = "insert success, last id"
GLOBAL._bear_msg_10006 = 10006;
GLOBAL._bear_msg_10006_text = "row found id";
GLOBAL._bear_msg_10007 = 10007;
GLOBAL._bear_msg_10007_text = "no row found id";
GLOBAL._bear_msg_19999 = 19999;
GLOBAL._bear_msg_19999_text = "message not available";
GLOBAL._bear_callback = function(callback, iok, istatus, iresult, imessage, imdata) {
  var msg = "";
  switch (imessage) {
  case GLOBAL._bear_msg_10001:
    msg = GLOBAL._bear_msg_10001_text + " (" + GLOBAL._bear_schema + "." + GLOBAL._bear_table + ")";
    break;
  case GLOBAL._bear_msg_10002:
    msg = GLOBAL._bear_msg_10002_text + " (" + GLOBAL._bear_schema + "." + GLOBAL._bear_table + ")";
    break;
  case GLOBAL._bear_msg_10003:
    msg = GLOBAL._bear_msg_10003_text + " (" + GLOBAL._bear_schema + "." + GLOBAL._bear_table + ")";
    break;
  case GLOBAL._bear_msg_10004:
    msg = GLOBAL._bear_msg_10004_text + " (" + imdata + ")";
    break;
  case GLOBAL._bear_msg_10006:
    msg = GLOBAL._bear_msg_10006_text + " (" + imdata + ")";
    break;
  case GLOBAL._bear_msg_10007:
    msg = GLOBAL._bear_msg_10007_text + " (" + imdata + ")";
    break;
  default:
    msg = GLOBAL._bear_msg_19999_text + " (" + imessage + ")";
    break;
  }
  var rsp = {
          "ok":iok,
          "status":istatus,
          "message":msg,
          "result":iresult
         };
  callback(rsp);
};
GLOBAL._bear_callback_200 = function(callback, iresult, imessage, imdata) {
  GLOBAL._bear_callback(callback, true, 200, iresult, imessage, imdata);
}
GLOBAL._bear_callback_201 = function(callback, iresult, imessage, imdata) {
  GLOBAL._bear_callback(callback, true, 201, iresult, imessage, imdata);
}
GLOBAL._bear_callback_404 = function(callback, iresult, imessage, imdata) {
  GLOBAL._bear_callback(callback, false, 404, iresult, imessage, imdata);
}

// bear cache (150/hits/second to 2000+/hits/second)
// =============================================================================
GLOBAL._bear_cache_list = "deadbeef";
GLOBAL._bear_cache_expire = 3;
BearCache = function() {
  this.cache = {};
  this.t1 = new Date();
  this.expire = function(pthis) {
    // expire cache setting seconds 
    var t2 = new Date();
    var difference = (t2 - pthis.t1) / 1000;
    if (difference > GLOBAL._bear_cache_expire) {
      pthis.cache = {};
      pthis.t1 = new Date();
    }
  }
}
BearCache.prototype.add = function(key, query) {
  this.cache[key] = query;
}
BearCache.prototype.exist = function(key) {
  this.expire(this);
  if (this.cache[key] == undefined) {
    return false;
  }
  return true;
}
BearCache.prototype.find = function(key) {
  this.expire(this);
  if (this.cache[key] == undefined) {
    return null;
  }
  return this.cache[key];
}
GLOBAL._bear_cache = new BearCache();

// bear connection
// =============================================================================
BearConn = function(idx) {
  this.conn = new GLOBAL._bear_db.dbconn();
  this.conn.conn(GLOBAL._bear_database);
  this.inuse = false;
  this.schema = GLOBAL._bear_schema;
  this.table = GLOBAL._bear_table;
  this.idx = idx;
  this.stmt = new GLOBAL._bear_db.dbstmt(this.conn);
}
BearConn.prototype.free = function() {
  var newstmt = new GLOBAL._bear_db.dbstmt(this.conn); // different stmt nbr than delete below (debug)
  if (this.stmt) {
    delete this.stmt;
  }
  this.stmt = newstmt;
}
BearConn.prototype.detach = function() {
  var newstmt = new GLOBAL._bear_db.dbstmt(this.conn); // different stmt nbr than delete below (debug)
  if (this.stmt) {
    delete this.stmt;
  }
  this.stmt = newstmt;
  this.inuse = false;
}
BearConn.prototype.getInUse = function() {
  return this.inuse;
}
BearConn.prototype.setInUse = function() {
  this.inuse = true;
}

// bear connection pool
// =============================================================================
BearPool = function() {
  this.pool = [];
  this.pmax = 0;
}
BearPool.prototype.attach = function(callback) {
  var valid_conn = false;
  while (!valid_conn) {
    // find available connection
    for(var i = 0; i < this.pmax; i++) {
      var inuse = this.pool[i].getInUse();
      if (!inuse) {
        this.pool[i].setInUse();
        callback(this.pool[i]);
        return;
      }
    }
    // expand the connection pool
    var j = this.pmax;
    for(var i = 0; i < GLOBAL._bear_pool_conn_incr_size; i++) {
      this.pool[j] = new BearConn(j);
      j++;
    }
    this.pmax += GLOBAL._bear_pool_conn_incr_size;
  }
}

// a bear
// =============================================================================

// Bear ctor
Bear = function() {
  this.bpool = new BearPool();
  // bad news
  this.bearDead = function(pthis) { 
    throw new Error("(" + GLOBAL._bear_schema + "." + GLOBAL._bear_table + ")");
  }
  // Is table bears.bear ok?
  this.bearCheck = function (pthis, callback) {
    pthis.bpool.attach( function(bconn) {
      var sql = "SELECT TABLE_OWNER, TABLE_NAME FROM QSYS2.SYSTABLES where TABLE_NAME='" + bconn.table + "'";
      try {
        bconn.stmt.execSync(sql, function (result) {
          bconn.detach();
          if (result[0] == undefined) {
            callback(pthis);
          }
        });
      } catch(ex) {
        bconn.detach();
        callback(pthis);
      }
    });
  }
  // create table bears.bear
  this.bearTable = function (pthis) {
    // Create table bears.bear
    pthis.bpool.attach( function(bconn) {
      var sql = "CREATE TABLE " + bconn.schema + "." + bconn.table 
              + "(id INTEGER GENERATED BY DEFAULT AS IDENTITY (START WITH 1) PRIMARY KEY, name varchar(256) not null)";
      try {
        bconn.stmt.execSync(sql, function (result) {
          bconn.detach();
        });
      } catch(ex) {
        bconn.detach();
      }
    });
    // Is table bears.bear ok?
    pthis.bearCheck(pthis, pthis.bearDead);
  }
  // create table bears.bear
  this.bearSchema = function (pthis) {
    // Create schema
    pthis.bpool.attach( function(bconn) {
      var sql = "CREATE SCHEMA " + bconn.schema;
      try {
        bconn.stmt.execSync(sql, function (result) { 
          bconn.detach(); 
        });
      } catch(ex) {
        bconn.detach();
      }
    });
    // Is table bears.bear ok?
    pthis.bearCheck(pthis, pthis.bearTable);
  }
  // check or create table bears.bear
  this.bearCheck(this, this.bearSchema);
};

// select all rows bears.bear
// Bear.find(function(response){});
// GET        Read            200 (OK), list of customers. Use pagination,  200 (OK), single customer.
//                                sorting and filtering to navigate         404 (Not Found), if ID not found or invalid.
//                                big lists.
Bear.prototype.find = function(callback) {
  var exist = GLOBAL._bear_cache.exist(GLOBAL._bear_cache_list);
  if (exist) {
    var found = GLOBAL._bear_cache.find(GLOBAL._bear_cache_list);
    if (found) {
      GLOBAL._bear_callback_200(callback,found,GLOBAL._bear_msg_10002,null);
    } else {
      GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10003,null);
    }
    return;
  }
  this.bpool.attach( function(bconn) {
    var sql = "select * from " + bconn.schema + "." + bconn.table + " order by name";
    try {
      bconn.stmt.exec(sql, function (query) {
        bconn.detach();
        if (query[0] == undefined) {
          GLOBAL._bear_cache.add(GLOBAL._bear_cache_list, false);
          GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10003,null);
        } else {
          GLOBAL._bear_cache.add(GLOBAL._bear_cache_list, query);
          GLOBAL._bear_callback_200(callback,query,GLOBAL._bear_msg_10002,null);
        }
      });
    } catch(ex) {
      bconn.detach();
      GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10001,null);
    }
  });
}

// add row bears.bear
// Bear.save(myname, function(response){});
// POST       Create          201 (Created), 'Location' header with link    404 (Not Found).
//                                to /customers/{id} containing new ID.     409 (Conflict) if resource already exists.
Bear.prototype.save = function(myname, callback) {
  this.bpool.attach( function(bconn) {
    var sql = "insert into " + bconn.schema + "." + bconn.table + " (NAME) VALUES('" + myname +"')";
    try {
      bconn.stmt.exec(sql, function (query) {
        bconn.free();
        try {
          sql = "SELECT IDENTITY_VAL_LOCAL() FROM SYSIBM.SYSDUMMY1";
          bconn.stmt.exec(sql, function (query) {
            bconn.detach();
            if (query[0] == undefined) {
              GLOBAL._bear_callback_201(callback,false,GLOBAL._bear_msg_10004,"?");
            } else {
              GLOBAL._bear_callback_201(callback,false,GLOBAL._bear_msg_10004,query[0]["00001"]);
            }
          });
        } catch(ex) { 
          bconn.detach();
          GLOBAL._bear_callback_201(callback,false,GLOBAL._bear_msg_10004,"?");
        }
      });
    } catch(ex) {
      bconn.detach();
      GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10001,null);
    }
  });
}

// select by id row bears.bear
// Bear.findById(myid, function(response){});
// GET        Read            200 (OK), list of customers. Use pagination,  200 (OK), single customer.
//                                sorting and filtering to navigate         404 (Not Found), if ID not found or invalid.
//                                big lists.
Bear.prototype.findById = function(myid, callback) {
  var exist = GLOBAL._bear_cache.exist(myid);
  if (exist) {
    var found = GLOBAL._bear_cache.find(myid);
    if (found) {
      GLOBAL._bear_callback_200(callback,found,GLOBAL._bear_msg_10006,myid);
    } else {
      GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10007,myid);
    }
    return;
  }
  this.bpool.attach( function(bconn) {
    var sql = "select * from " + bconn.schema + "." + bconn.table + " where ID=" + myid;
    try {
      bconn.stmt.exec(sql, function (query) {
        bconn.detach();
        if (query[0] == undefined) {
          GLOBAL._bear_cache.add(myid, false);
          GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10007,myid);
        } else {
          GLOBAL._bear_cache.add(myid, query);
          GLOBAL._bear_callback_200(callback,query,GLOBAL._bear_msg_10006,myid);
        }
      });
    } catch(ex) {
      bconn.detach();
      GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10001,null);
    }
  });
}

// remove by id row bears.bear
// Bear.removeById(myid, function(response){});
// DELETE     Delete          404 (Not Found), unless you want to           200 (OK).
//                                delete the whole collection               404 (Not Found), if ID not found or invalid.
Bear.prototype.removeById = function(myid, callback) {
  this.bpool.attach( function(bconn) {
    var sql = "select * from " + bconn.schema + "." + bconn.table + " where ID=" + myid;
    try {
      bconn.stmt.exec(sql, function (query) {
        bconn.free();
        if (query[0] == undefined) {
          bconn.detach();
          GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10007,myid);
        } else {
          var sql = "delete from " + bconn.schema + "." + bconn.table + " where ID=" + myid;
          try {
            bconn.stmt.exec(sql, function (query) {
              bconn.detach();
              GLOBAL._bear_callback_200(callback,false,GLOBAL._bear_msg_10006,myid);
            });
          } catch(ex) {
            bconn.detach();
            GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10001);
          }
        }
      });
    } catch(ex) {
      bconn.detach();
      GLOBAL._bear_callback_404(callback,false,GLOBAL._bear_msg_10001);
    }
  });
}


// debug: expose BearPool
// Bear.debugBearPool(function(pool){});
Bear.prototype.debugBearPool = function(callback) {
  callback(this.bpool);
}


exports.Bear = Bear;
// debug
exports.BearConn = BearConn;
exports.BearPool = BearPool;


