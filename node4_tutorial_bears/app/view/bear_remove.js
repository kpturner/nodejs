$(document).ready(function() {
  // bear remove
  var id = $.urlParam('id'); 
  $.delete("/api/bears/" + id, function(query) {
    $('.bear_id').text("(" + id + ")");
    $('.bear_message').text(query.message);
  })
  .error(function(query) { alert(JSON.stringify(query.responseJSON.message) ); }); // delete
}); // ready

