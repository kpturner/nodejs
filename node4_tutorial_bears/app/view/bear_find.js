$(document).ready(function() {
  // bear find list
  $.getJSON("/api/bears", function(query) {
    $('.bear_message').text(query.message);
    $.each(query.result, function (i, bear) {
        var option_bear = ('<li class="item"><a href="/bears/bear_id.html?id='+ bear['ID'] +'">' + bear['ID'] + '</a>'+ " " + bear['NAME'] + '</li>');
        $('.bear_list').append(option_bear);
      }); // each
    }) 
    .error(function(query) { alert(JSON.stringify(query.responseJSON.message)) }); // getJSON
}); // ready

