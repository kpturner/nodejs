$(document).ready(function() {
  // bear findById
  var id = $.urlParam('id'); 
  $.getJSON("/api/bears/" + id, function(query) {
    $('.bear_message').text(query.message);
    $.each(query.result, function (i, bear) {
      var option_bear = ('<li class="item">' + bear['ID'] + " - " + bear['NAME'] + " " + '<a href="/bears/bear_gone.html?id=' + bear['ID'] +'">' + "(remove)" + '</a>' + '</li>');
      $('.bear_list').append(option_bear);
    }); // each
  })
  .error(function(query) { alert(JSON.stringify(query.responseJSON.message)); }); // getJSON
}); // ready
