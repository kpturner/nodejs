# warning
Parts of this tutorial will not run without PTFs to db2a (not yet available). 
For example, I believe exec delete and save may hang without a PTF to db2a
(although GUI may still run).
This tutorial is stored for a demo IBM i group coming later next month.
The techniques are valid, and, can be applied to old db2i version interfaces
(maybe only missing db.init).

# node.js tutorial
This tutorial is a simple simulation of a project with customer requirements.
The customer intent is one service for both business REST JSON clients (other programs), 
and traditional browser customers.
A project constraint of two different teams was added, where JSON API people deal with the model (db2), and,
art centric web designer people deal with the view (browser). This is common for sites with theme, branding and such.
```
Project requirements.
> JSON API interface
> Browser interface
> Serve 2000/hits second
> Current LPAR hardware partition (small)
> Isolate Node.js from root of machine
> Integrated into existing Apache site
> Different teams API and GUI (browser)
> Use only JavaScript and html skills
```

*** notes ***

Based on:  https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4

Download available: https://bitbucket.org/litmis/nodejs/downloads


First, project is designed to develop and deploy within a chroot with an existing Apache site. You may choose to develop in the root file system. 
However, i suggest you follow the extra chroot steps to truly understand the benefits of nodejs development project separation.
That is, when you use npm to augment your chroot nodejs project, really nice to know you are not clobbering some other nodejs application.
Also, if you like, enjoy more confidence of knowing hackers are contained to chroot location (nothing really business interesting).

Node 4 PTFs for asynchronous calls are not all complete at this time. However project demonstrates a version of connection pooling technique.
Essentially, DB2 rule is never share a connection or statement across threads at same time. Unfortunately all previous examples do exactly wrong thing
using a single global connection, which will fail (horribly), when true async/callback db2 interfaces are available (*). (First ship version of 
node4 new db2a interfaces are not actually asynchronous. Aka, trace tool like libdb400.a trace from yips shows only main thread 
will be handling all CLI DB2. Team is working on truly async interfaces, so be patient.)

(*) Technical folks. Yes. Node.js is not threaded per say, instead uses an event loop main thread to process the 'script'. 
However, long running activities, mostly 'I/O' operations, nodejs extension c code provider convention 'long' operation
is 'run asynchronous ' in a worker thread away from main event loop to prevent main loop stalling (current db2 design stalls). 
Completion of 'run asynchronous ' in a worker thread is followed by a callback insertion into event loop (aka, script callback).
If you have a nodejs application with enough 'I/O' work (not db2a yet), you can observe wrkactjob, watching worker threads consume CPU.

Most nodejs projects (including mine), couple view with controller/model using the express, jade/pug. Nothing wrong with this idea. 
However, this project wants to include both browser clients and API JSON REST clients with few steps as possible. A separation of 
human tasks view/model allows for a demonstration of integrating view elements into existing Apache, 
wherein all view elements are handle by Apache (not nodejs). This enables high performance advances like Apache FRCA to be used
independently on view elements with no effect on nodejs model server 
(http://www.ibm.com/support/knowledgecenter/ssw_ibm_i_71/rzaie/rzaieconfrca.htm).

Last, cache is demonstrated in bear model to achieve customer requirements for 2000/hits a second (millions per hour). 
As IBM i people, database people, we at times become overly obsessed with data truth, to point of 
disadvantaging our own fine machine resources and database. The trick is to understand relevance of 
time sensitive data 'truth' must favor consumer experience to build better web sites 
that use less machine resources (including database). In this case, data is bear names, 
reality of the consumer experience is not even noticeably impacted 
by a 3 second memory cache truth (*).

(*) The odds of a cache lie are dramatically decreased by the fact that 99% of all data interactions will be read. 
That is to say, add or update of bear names is likely to be so rare that probability of a 
cache lie event occurring approaches zero. If you will allow, I suggest much of 
human interacting with read data is same, lumber yards, cars for sale, high school principles morning wiki entry, etc. 
Cache to win folks. 

Still think cache is evil? How about this scenario. Most of us order things on-line. Retail sites, 
list of stuff to buy pops up like they can't give it away fast enough. You shop, you click, scroll, click some more.
Last moment, you click check-out,  and, message comes up 'item #3 no longer available'. Welcome to cache world of white lie. 
If i may, funny part, when unlikely cache lie occurs, people are already trained to refresh the screen, 
re-check items, often takes more than 3 seconds as they fumble with cell phone application 
(... application navigation by design ... wink, wink).    

# complete project materials
This code is not perfect, but demonstrates a whole lot of excellent technique to save endless hours of 're-factoring'.
If i may, re-factoring evokes memories of lost sleep and explaining to your boss. Maybe this little sample will help. 
You decide. 
```
./
server_step_1.js -- basic express server
server_step_3.js -- api routes server
app/model/
bear_step_2.js -- basic bear model (80/hits second)
bear_step_4.js -- read performance cache (2000/hits second)
bear_step_optional_add_old_db2_driver.js -- optional use old db2 interface driver
test/
test_bear_ctor.js -- bear database create
test_bear_save.js -- create bear record
test_bear_find.js -- find all bear records
test_bear_findById.js nbr -- find bear record by id
test_bear_removeById.js nbr -- remove bear record by id
test_stress_findById.js nbr -- stress test find bear record by id
conf/
httpd.conf -- proxy/reverse to node.js server.js
app/view
index.html -- list bears
bear_find.js -- list bears create
bear_add.js -- add a bear name
bear_findById.js -- single bear
bear_id.html -- view single bear action
bear_gone.html -- remove bear
bear_remove.js -- remove bear delete
helper.js -- helper js functions
bear.css -- view layout
```

# LPP OPS group PTF (node version 4.4.6)
https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/IBM%20i%20Technology%20Updates/page/Open%20Source%20Technologies
```
> WRKPTFGRP PTFGRP(SF99123) - 7.1 level 1
> WRKPTFGRP PTFGRP(SF99223) - 7.2 level 1
> WRKPTFGRP PTFGRP(SF99225) - 7.3 level 1
> DSPPTF LICPGM(5733OPS) SELECT(SI61142)
> DSPPTF LICPGM(5733OPS) SELECT(SI61394)
```

# chroot set-up (profile-admin)
Project is designed to develop and deploy within a chroot with an existing Apache site. You may choose to develop in the root file system. 
However, i suggest you follow the extra chroot steps to truly understand the benefits of nodejs development project separation.
That is, when you use npm to augment your chroot nodejs project, really nice to know you are not clobbering some other nodejs application.

*** download latest ibmichroot ***
```
https://bitbucket.org/litmis/ibmichroot/downloads
$ ftp myibmi
ftp> bin
ftp> cd /home/(profile-admin)
ftp> put litmis-ibmichroot-xxxb.zip
ftp> quit
```

*** create node4 chroot profile (IBM i use profile-admin) ***
```
5250 (profile-admin)
> CRTUSRPRF USRPRF(NODE4) PASSWORD() USRCLS(*PGMR) TEXT('Tony Cairns')
> CHGUSRPRF USRPRF(NODE4) LOCALE(*NONE) HOMEDIR('/QOpenSys/node4/./home/node4')
```

*** create node4 chroot (IBM i use profile-admin) ***
```
$ ssh -X (profile-admin).myibmi
$ ksh
$ export PATH=.:/QOpenSys/QIBM/ProdData/OPS/tools/bin:/QOpenSys/usr/bin
$ bash
$ mkdir -p /QOpenSys/node4/home/node4
$ unzip litmis-ibmichroot-de6baa0c254b.zip 
$ mv litmis-ibmichroot-de6baa0c254b ibmichroot
$ cd ibmichroot/chroot
$ ./chroot_setup.sh chroot_minimal.lst /QOpenSys/node4 
$ ./chroot_setup.sh chroot_nls.lst /QOpenSys/node4 
$ ./chroot_setup.sh chroot_OPS_SC1.lst /QOpenSys/node
$ ./chroot_setup.sh chroot_gen_OPS_tools.lst /QOpenSys/node4
$ ./chroot_setup.sh chroot_gen_OPS_Node4.lst /QOpenSys/node4
```

*** make node4 owner of new chroot (IBM i use profile-admin) ***
```
$ chroot /QOpenSys/node4 /QOpenSys/usr/bin/ksh
$ export PATH=.:/QOpenSys/QIBM/ProdData/OPS/Node4/bin:/QOpenSys/QIBM/ProdData/OPS/tools/bin:/QOpenSys/usr/bin
$ bash
$ cd /
$ chown -Rh node4 /
$ exit
$ exit
$ pwd
/home/(profile-admin)/ibmichroot/chroot
```

# chroot build node4 app (node4 profile)
Our first task, npm load express and body-parser into our project. Specifically npm utility is downloading code from the internet.
This means your IBM i must be able to reach outside your company firewall to complete the task. Also, perhaps obvious,
you are trusting the code downloaded to behave appropriately in your web site (another reason i like chroot applications).

*** ssh to new chroot (node4 profile) ***
```
$ ssh -X node4@ut28p63
node4@ut28p63's password: 
$ ksh
$ export PATH=.:/QOpenSys/QIBM/ProdData/OPS/Node4/bin:/QOpenSys/QIBM/ProdData/OPS/tools/bin:/QOpenSys/usr/bin
$ bash
$ node --version
v4.4.6
$ 
```

*** Defining our Node Packages package.json ***
```
$ cat package.json 
{
    "name": "node-api",
    "main": "server.js",
    "dependencies": {
        "express": "~4.0.0",
        "body-parser": "~1.0.1"
    }
}
```

*** Installing Our Node Packages ***
```
$ npm install
bash-4.3$ npm install
npm WARN package.json node-api@ No repository field.
npm WARN package.json node-api@ No license field.
body-parser@1.0.2 node_modules/body-parser
:
express@4.0.0 node_modules/express
:
```

# server_step_1.js - Initial server
First step in a good project, get anything to work. 
A good feeling when you see your browser connect to the project right off.

*** Setting Up Our Server server.js ***
```
$ cp server_step_1.js server.js
```

*** Starting Our Server and Testing ***
```
$ node server.js 
Magic happens on port 8080

http://myibmi:8080/api
{"message":"hooray! welcome to our api!"}
```


# bear_step_2.js - Initial REST model
Testing model before putting it all together is very useful. There are fine testing frameworks available, please use them.
However, for this simple tutorial a few custom tests fit my needs nicely.
```
$ cp app/models/bear_step_2.js app/models/bear.js
```

*** Testing ***
```
$ node test/test_bear_ctor.js
$ node test/test_bear_save.js
$ node test/test_bear_find.js
$ node test/test_bear_findById.js nbr
$ node test/test_bear_removeById.js nbr
```

# server_step_3.js - REST route and view
The tutorial tests with curl to act as a view component to check out REST JSON API. You could use any other 
REST capable language to consume JSON APIs bear project (php, ruby, python, etc.). Use of standard REST http
verbs like GET, POST, DELETE, etc., enables consistency across languages. When you move on to html/javascript
interface, you will find jQuery and other javascript elements exactly the same. All language API REST clients served with one
nodejs service (including browser), per customer requirement.
```
$ cp server_step_3.js server.js
$ node server.js 
Magic happens on port 8080
```

*** Testing ***
```
$ curl http://ut28p63:8080/api
{"message":"hooray! welcome to our api!"}

GET list
$ curl http://ut28p63:8080/api/bears
[{"ID":"2","NAME":"Brown"},{"ID":"3","NAME":"Brown"}

GET by id
$ curl http://ut28p63:8080/api/bears/3
[{"ID":"3","NAME":"Brown"}]

POST save name
$ curl -d "name=Sally" http://ut28p63:8080/api/bears

DELETE by id
$ curl -X DELETE http://ut28p63:8080/api/bears/3
```


# bear_step_4.js - Cache for performance
Bear cache is demonstrated in bear model to achieve customer requirements for 2000/hits a second (millions per hour). 
The trick is to understand relevance of time sensitive data 'truth' must favor consumer experience to build better web sites 
that use less machine resources (including database).
```
$ node server.js 
Magic happens on port 8080
```

*** before cache (ab tool test) ***
```
$ ab -t 15 -c 60 http://ut28p63:8080/api/bears/2
Concurrency Level:      60
Time taken for tests:   15.072 seconds
Complete requests:      1351
Failed requests:        0
Write errors:           0
Total transferred:      276606 bytes
HTML transferred:       37719 bytes
Requests per second:    89.64 [#/sec] (mean)
Time per request:       669.361 [ms] (mean)
Time per request:       11.156 [ms] (mean, across all concurrent requests)
Transfer rate:          17.92 [Kbytes/sec] received
```

*** copy model cache code ***
```
$ cp app/models/bear_step_4.js app/models/bear.js
```

*** after cache (ab tool test) ***
```
$ ab -t 15 -c 60 http://ut28p63:8080/api/bears/2                               
Concurrency Level:      60
Time taken for tests:   15.003 seconds
Complete requests:      38197
Failed requests:        0
Write errors:           0
Total transferred:      7563006 bytes
HTML transferred:       1031319 bytes
Requests per second:    2545.91 [#/sec] (mean)
Time per request:       23.567 [ms] (mean)
Time per request:       0.393 [ms] (mean, across all concurrent requests)
Transfer rate:          492.28 [Kbytes/sec] received
```

# view (admin -- outside chroot)
The Apache configuration below is a bit tricky. Look closely at /www/instance/htdocs symbolic links back 
into nodejs bear app/view project chroot location. Also take careful note of httpd.conf proxy/reverse proxy directory /api/.
This can be added to any existing Apache instance. Also sets a pattern for many nodejs chroot applications to be added to
any given Apache site.

*** config ***
```
> cp /QOpenSys/node4/home/node4/node4_tutorial_bears/conf/httpd.conf /www/apachedft/conf/.
> cd /www/apachedft/htdocs
> ln -sf /QOpenSys/node4/home/node4/node4_tutorial_bears/app/view bears
bash-4.3$ ls -l
lrwxrwxrwx    1 adc      0               112 Oct 20 10:06 bears -> /QOpenSys/node4/home/node4/node4_tutorial_bears/app/view
-rwx---r-x    1 qsys     0              1008 Oct  7 2009  index.html
```

*** run (browser) ***
```
http://ut28p63.rch.stglabs.ibm.com/bears/
```

# bear_step_optional_add_old_db2_driver.js - Optional try example with old db2 driver
A mapping class included in bear model should you want to use the old db2 driver.

*** optional step ***

```
$ export BEAR_DB_VERSION=1
$ node server.js          
moldy old db2 no-async
Magic happens on port 8080

$ unset BEAR_DB_VERSION 
$ node server.js       
new improved db2 async
Magic happens on port 8080
``` 






